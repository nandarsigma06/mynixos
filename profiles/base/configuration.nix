{ config, pkgs, lib, wm, mainLocale, extraLocale, timeZone, username, name, profile, ... }:

{
  imports =
    [
      ../../system/hardware
      ../../system/wm/${wm}.nix
      ../../system/app/flatpak.nix
      #../../system/app/docker.nix
      #( import ../../system/app/docker.nix {storageDriver = "aufs"; inherit username pkgs config lib;} )
      ../../system/app/fish.nix
      #../../system/app/steam.nix
      ../../system/security/privilege_escalation/doas.nix
      ../../system/security/firewall.nix
      ../../system/security/gpg.nix
      #../../system/security/mac-changer.nix
      ../../system/extras/earlyoom.nix


    ];

  networking.hostName = "${profile}"; # Define your hostname.

  time.timeZone = timeZone;

  i18n = {
    defaultLocale = mainLocale;
    extraLocaleSettings = {
      LC_ADDRESS = extraLocale;
      LC_IDENTIFICATION = extraLocale;
      LC_MEASUREMENT = extraLocale;
      LC_MONETARY = extraLocale;
      LC_NAME = extraLocale;
      LC_NUMERIC = extraLocale;
      LC_PAPER = extraLocale;
      LC_TELEPHONE = extraLocale;
      LC_TIME = extraLocale;
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.${username} = {
    isNormalUser = true;
    description = name;
    extraGroups = [ "networkmanager" "wheel" ];
    packages = [];
    uid = 1000;
    initialPassword = "1988";
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # System Packages
  environment.systemPackages = with pkgs; [
    wget
    git
    eza
    fd
    ripgrep
    zsh
    btop
    htop
    bash
    home-manager
    mpv
    neofetch
    ntfs3g
    tree

  ];
  # Configure keymap in X11
 services.xserver = {
   layout = lib.mkDefault "us";
   xkbVariant = "";
 };
  # udisk2
  services = {
    udisks2 = {
      enable = true;
    };
  };
  
  services.gvfs.enable = true;
  services.devmon.enable = true;
  nix = {
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
      substituters = ["https://hyprland.cachix.org"];
      trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
    };
    optimise.automatic = true;
  };

  system.stateVersion = "23.05";
}
