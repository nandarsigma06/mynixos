{
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    ./amd-gpu.nix
    ./android.nix
    ./bluetooth.nix
    ./bootloader.nix
    ./console.nix
    ./kernel.nix
    ./networking.nix
    ./printing.nix
    ./sound.nix
  ];
}