{ config, pkgs, ... }:

{
  networking.networkmanager.enable = true;
  networking.networkmanager.dns = "default";
}
