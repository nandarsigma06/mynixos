{ config, pkgs, ... }:

{
  hardware.opengl.driSupport32Bit = true;
  programs.steam.enable = false;
  #environment.systemPackages = with pkgs; [ steam ];
}
