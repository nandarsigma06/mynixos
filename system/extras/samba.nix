{ config, pkgs, lib, ... }:

{
services.samba-wsdd = {
  # make shares visible for windows 10 clients
  enable = true;
  openFirewall = false;
};
services.samba = {
  enable = true;
  securityType = "user";
  extraConfig = ''
    workgroup = WORKGROUP
    server string = smbnix
    netbios name = smbnix
    security = user 
    #use sendfile = yes
    max protocol = smb2
    # note: localhost is the ipv6 localhost ::1
    hosts allow = 192.168.0. 127.0.0.1 localhost
    hosts deny = 0.0.0.0/0
    guest account = nobody
    map to guest = bad user
  '';
  shares = {
    public = {
      path = "/home/nandar/Data";
      browseable = "yes";
      "read only" = "no";
      "guest ok" = "yes";
      "create mask" = "0644";
      "directory mask" = "0755";
      "force user" = "nandar";
      "force group" = "smbgroup";
    };
   #private = {
   #  path = "/mnt/Shares/Private";
   #  browseable = "yes";
   #  "read only" = "no";
   #  "guest ok" = "no";
   #  "create mask" = "0644";
   #  "directory mask" = "0755";
   #  "force user" = "username";
   #  "force group" = "groupname";
   #};
  };
};
}
