{

  ...
}: {
  security.polkit.enable = true;
  environment = {
    sessionVariables = {
      MOZ_ENABLE_WAYLAND = "1";
      MOZ_USE_XINPUT2 = "1";
      XDG_SESSION_TYPE = "wayland";
      XDG_CURRENT_DESKTOP = "hyprland";
      SDL_VIDEODRIVER = "wayland";
      QT_QPA_PLATFORM="wayland";
      QT_QPA_PLATFORMTHEME = "qt5ct";
      QT_WAYLAND_DISABLE_WINDOWDECORATION="1";
    };

  };
}
