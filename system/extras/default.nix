{ ... }: {
  imports = [
    ./dbus.nix
    ./display-manager.nix
    ./fonts.nix
    ./keyring.nix
    ./wayland.nix
    ./xdg.nix
    ./earlyoom.nix
    ./environment.nix
    ./samba.nix
   # ./espanso.nix
  ];
}
