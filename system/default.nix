{ ... }: {
  imports = [
    ./hardware
    ./security
    ./wm
    ./extras
    ./app
   ];
}