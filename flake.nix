{
  description = "Flake for Henrique's system";

  outputs = { nixpkgs, home-manager, rust-overlay, nix-vscode-extensions, hyprland, ... } @ inputs :
  let
    ### OPTIONS
    # System Options
    system = "x86_64-linux";
    profile = "laptop";
    timeZone = "Asia/Makassar";
    mainLocale = "en_US.UTF-8";
    extraLocale = "id_ID.UTF-8";

    # User options
    username = "nandar";
    name = "Risnandar";
    email = "nandarsigma06@gmail.com";
    # theme = "uwunicorn-yt";
    wm = "hyprland";
    terminal = "kitty";
    browser = "firefox-esr";
    mainEditor = "codium";
    editor = "nvim";
    dotfilesDir = "~/.dotfiles";

    ### DERIVED ARGUMENTS
    someArgs = {
      inherit timeZone;
      inherit mainLocale;
      inherit extraLocale;
      inherit wm;
      inherit username;
      inherit name;
      inherit email;
      inherit terminal;
      inherit browser;
      inherit mainEditor;
      inherit editor;
      inherit dotfilesDir;
      inherit inputs;
      inherit profile;
    };
    profilePath = ./profiles/${profile};

    lib = nixpkgs.lib;
    pkgs = import nixpkgs {
      inherit system;
      config = {
        allowUnfree = true;
        android_sdk.accept_license = true;
      };
      overlays = [ rust-overlay.overlays.default nix-vscode-extensions.overlays.default ];
    };
  in {
    # NixOS System Configuration
    nixosConfigurations.system = nixpkgs.lib.nixosSystem {
      inherit system;
      modules = [ ( profilePath + "/configuration.nix") ];
      specialArgs = (someArgs // { inherit lib; });
    };

    # Home Manager Configuration
    homeConfigurations.user = home-manager.lib.homeManagerConfiguration {
      inherit pkgs;
      modules = [ hyprland.homeManagerModules.default ( profilePath + "/home.nix") ];
      extraSpecialArgs = someArgs;
    };
  };


  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay.url = "github:oxalica/rust-overlay";
    nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";
    hyprland.url = "github:hyprwm/Hyprland";
    hyprland-plugins = {
      url = "github:hyprwm/hyprland-plugins";
      inputs.hyprland.follows = "hyprland";
    };
  };
}
