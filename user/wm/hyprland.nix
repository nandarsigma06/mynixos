{ config, pkgs, lib, terminal, browser, mainEditor, inputs, ... }:

let
  winWrapClass = "terminal-background";
  winWrapBinName = "cava-wrap";
  winWrapBin = ''
    #!/bin/sh
    sleep 0.5 && cava
  '';
  winWrapConfigFile = "hypr/kitty-${winWrapClass}.conf";
  winWrapConfig = ''
    background_opacity 0.0
  '';
in {
  imports = [
    ../app/browser/${browser}.nix
    ../app/terminal/${terminal}.nix
    ../app/editor/${mainEditor}.nix
    ../app/utils/rofi/rofi.nix
    ../app/utils/dunst.nix
    ../app/utils/waybar.nix
  ];

  #home.file.".config/hypr/cw.sh".{source =./cw.sh; executable=true;};
  home.packages = with pkgs; [
    grimblast swww wl-clipboard
    (pkgs.writeScriptBin winWrapBinName winWrapBin)
  ];
  xdg.configFile.${winWrapConfigFile}.text = winWrapConfig;

  programs.cava = {
    enable = true;
    settings = {
      general = {
        mode = "normal";
        framerate = 60;
      };
      input = {
        method = "pipewire";
        source = "auto";
      };
      output.method = "noncurses";
      color = {
        gradient = 1;
        gradient_count = 8;
        gradient_color_1 = "'#bf616a'";
        gradient_color_2 = "'#d08770'";
        gradient_color_3 = "'#ebcb8b'";
        gradient_color_4 = "'#a3be8c'";
        gradient_color_5 = "'#b48ead'";
        gradient_color_6 = "'#88c0d0'";
        gradient_color_7 = "'#81a1c1'";
        gradient_color_8 = "'#5e81ac'";
      };
      smoothing.noise_reduction = 88;
    };
  };
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;
    plugins = [
      inputs.hyprland-plugins.packages.${pkgs.system}.hyprwinwrap
    ];
    extraConfig = ''
      env = LIBVA_DRIVER_NAME,nvidia
      env = XDG_SESSION_TYPE,wayland
      env = GBM_BACKEND,nvidia-drm
      env = __GLX_VENDOR_LIBRARY_NAME,nvidia
      env = WLR_NO_HARDWARE_CURSORS,1
      env = XCURSOR_SIZE,24
      env = QT_QPA_PLATFORM,wayland;xcb
      env = QT_QPA_PLATFORMTHEME,qt6ct
      env = GDK_BACKEND,wayland,x11
      env = SDL_VIDEODRIVER,wayland
      env = MOZ_ENABLE_WAYLAND,1
      env = MOZ_DRM_DEVICE,/dev/dri/renderD128

      exec-once = swww init
      exec-once = cw.sh
      exec-once = waybar
      exec-once = systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
      exec-once = dbus-launch --exit-with-session
      exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
      exec-once = /usr/lib/polkit-kde-authentication-agent-1
      exec-once = dunst
      exec-once = hyprctl setcursor ${config.gtk.cursorTheme.name} ${builtins.toString config.gtk.cursorTheme.size}
      exec-once = discordcanary --start-minimized
      exec-once = kitty -c "$XDG_CONFIG_HOME/${winWrapConfigFile}" --class="${winWrapClass}" ${winWrapBinName}
      exec-once = wl-paste --type text --watch cliphist store #Stores only text data
      exec-once = wl-paste --type image --watch cliphist store #Stores only image data
      input {
          kb_layout = us
          follow_mouse = 1

          touchpad {
            natural_scroll = no
          }

          sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
          force_no_accel = 1

	        float_switch_override_focus=0
      }

      general {
          gaps_in = 4
          gaps_out = 4
          border_size = 2
          col.active_border = rgb(b48ead)
          col.inactive_border = rgb(4c566a)

          layout = dwindle
      }

      decoration {
          rounding = 5

          blur {
              enabled = true
              size = 3
              passes = 2
              new_optimizations = true
          }
          #blur = true
          #blur_size = 3
          #blur_passes = 2
          #blur_new_optimizations = true

          drop_shadow = true
          shadow_range = 2
          shadow_render_power = 3
          col.shadow = rgba(1a1a1aee)
      }

      animations {
        enabled = yes
        bezier = wind, 0.05, 0.9, 0.1, 1.05
        bezier = winIn, 0.1, 1.1, 0.1, 1.1
        bezier = winOut, 0.3, -0.3, 0, 1
        bezier = liner, 1, 1, 1, 1
        animation = windows, 1, 6, wind, slide
        animation = windowsIn, 1, 6, winIn, slide
        animation = windowsOut, 1, 5, winOut, slide
        animation = windowsMove, 1, 5, wind, slide
        animation = border, 1, 1, liner
        animation = borderangle, 1, 30, liner, loop
        animation = fade, 1, 10, default
        animation = workspaces, 1, 5, wind
      }

      dwindle {
          pseudotile = true # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
          preserve_split = true # you probably want this
      }

      master {
          new_is_master = false
      }

      gestures {
          workspace_swipe = true
      }

      misc {
          vfr = true
          enable_swallow = true
          swallow_regex = ^(?:Alacritty|kitty)$
      }

      plugin {
        hyprwinwrap {
          class = ${winWrapClass}
        }
      }

      windowrulev2 = float,class:^(firefox)$,title:^(Picture-in-Picture)$
      windowrulev2 = float,title:^(ssh-askpass)$
      windowrulev2 = float,class:^(org.kde.polkit-kde-authentication-agent-1)$,title:^(Picture-in-Picture)$
      windowrulev2 = float,class:^(firefox)$,title:^(?:Firefox — Sharing Indicator|Firefox — Indicador de compartilhamento)$
      windowrulev2 = move 931 1049,class:^(firefox)$,title:^(?:Firefox — Sharing Indicator|Firefox — Indicador de compartilhamento)$
      windowrulev2 = opacity 0.98 0.95,class:^(?:Code|VSCodium|codium-url-handler|WebCord|code-url-handler|discord)$
      windowrule = windowdance,title:^(Rhythm Doctor)$

      windowrule = float,udiskie
      windowrule = float,title:^(Transmission)$
      windowrule = float,title:^(Volume Control)$
      windowrule = float,title:^(Firefox — Sharing Indicator)$
      windowrule = move 0 0,title:^(Firefox — Sharing Indicator)$
      windowrule = size 700 450,title:^(Volume Control)$
      windowrule = move 40 55%,title:^(Volume Control)$
      windowrulev2 = float, title:^(Picture-in-Picture)$
      windowrulev2 = opacity 1.0 override 1.0 override, title:^(Picture-in-Picture)$
      # windowrulev2 = opacity 1.0 override 1.0 override, title:^(.*YouTube.*)$
      windowrulev2 = pin, title:^(Picture-in-Picture)$
      windowrule = float,imv
      windowrule = center,imv
      windowrule = size 1200 725,imv
      windowrulev2 = opacity 1.0 override 1.0 override, title:^(.*imv.*)$
      windowrule = float,mpv
      windowrule = center,mpv
      windowrulev2 = opacity 1.0 override 1.0 override, title:^(.*mpv.*)$
      windowrule = tile,Aseprite
      windowrulev2 = opacity 1.0 override 1.0 override, class:(Aseprite)
      windowrulev2 = opacity 1.0 override 1.0 override, class:(Unity)
      windowrule = size 1200 725,mpv
      windowrulev2 = idleinhibit focus, class:^(mpv)$
      windowrulev2 = idleinhibit fullscreen, class:^(firefox)$

      windowrule = float,title:^(float_kitty)$
      windowrule = center,title:^(float_kitty)$
      windowrule = size 950 600,title:^(float_kitty)$

      windowrulev2 = float,class:^(pavucontrol)$
      windowrulev2 = float,class:^(SoundWireServer)$
      windowrulev2 = float,class:^(file_progress)$
      windowrulev2 = float,class:^(confirm)$
      windowrulev2 = float,class:^(dialog)$
      windowrulev2 = float,class:^(download)$
      windowrulev2 = float,class:^(notification)$
      windowrulev2 = float,class:^(error)$
      windowrulev2 = float,class:^(confirmreset)$
      windowrulev2 = float,title:^(Open File)$
      windowrulev2 = float,title:^(branchdialog)$
      windowrulev2 = float,title:^(Confirm to replace files)$
      windowrulev2 = float,title:^(File Operation Progress)$




      $mainMod = SUPER

      bind = $mainMod, Q, exec, ${terminal}
      bind = $mainMod, B, exec, ${browser}
      bind = $mainMod, V, exec, ${mainEditor}
      bind = $mainMod, X, killactive,
      bind = $mainMod, E, exec, thunar
      bind = ALT, Z, exit,
      bind = $mainMod, Space, togglefloating,
      bind = $mainMod, F, fullscreen,
      bind = $mainMod, P, exec, rofi -show run
      bind = $mainMod, A, exec, rofi -show drun
      bind = ALT,Tab,cyclenext,
      bind = ALT,Tab,bringactivetotop,
      bind = , Print, exec, grimblast copy area
      bind = ALT, K, exec, hyprctl kill
      bind = $mainMod, R, exec, kitty -e ranger
      bind = $mainMod, D, exec, kitty -e lf
      # Move focus with mainMod + arrow keys
      bind = $mainMod, left, movefocus, l
      bind = $mainMod, right, movefocus, r
      bind = $mainMod, up, movefocus, u
      bind = $mainMod, down, movefocus, d
      bind = $mainMod, Z, exec, rofi -show power-menu -modi power-menu:rofi-power-menu
      bind = $mainMod, O, exec, rofi -modi emoji -show emoji
      bind = $mainMod SHIFT, C, exec, rofi -show calc -modi calc -no-show-match -no-sort
      bind = $mainMod SHIFT, R, exec, rofi-screenshot
      bind = $mainMod ALT, R, exec, rofi-screenshot -s
      # bind = $mainMod, V, cliphist list | dmenu | cliphist decode | wl-copy
      bind = SUPER, V, exec, cliphist list | rofi -dmenu | cliphist decode | wl-copy
      bind = $mainMod SHIFT, P, exec, hyprpicker --autocopy
      bind = $mainMod, W, exec, rofi -show window
      bind = $mainMod, S, exec, grim -t jpeg -q 10 -g "$(slurp)" - | swappy -f - # take a screenshot
      bind = $mainMod, I, exec, kitty -e syncthing
      bind = $mainMod SHIFT, S, exec, kitty -e fzf
      bind = $mainMod, Y, exec, kitty -e yt-dlp

      #
      bind=,code:232,exec,brightnessctl set 15-
      bind=,code:233,exec,brightnessctl set +15
      bind=,code:122,exec,pamixer -d 10
      bind=,code:123,exec,pamixer -i 10

      # Switch workspaces with mainMod + [0-9]
      bind = $mainMod, 1, workspace, 1
      bind = $mainMod, 2, workspace, 2
      bind = $mainMod, 3, workspace, 3
      bind = $mainMod, 4, workspace, 4
      bind = $mainMod, 5, workspace, 5
      bind = $mainMod, 6, workspace, 6
      bind = $mainMod, 7, workspace, 7
      bind = $mainMod, 8, workspace, 8
      bind = $mainMod, 9, workspace, 9

      # Move active window to a workspace with ALT + [0-9]
      bind = ALT, 1, movetoworkspace, 1
      bind = ALT, 2, movetoworkspace, 2
      bind = ALT, 3, movetoworkspace, 3
      bind = ALT, 4, movetoworkspace, 4
      bind = ALT, 5, movetoworkspace, 5
      bind = ALT, 6, movetoworkspace, 6
      bind = ALT, 7, movetoworkspace, 7
      bind = ALT, 8, movetoworkspace, 8
      bind = ALT, 9, movetoworkspace, 9

      # Scroll through existing workspaces with mainMod + scroll
      bind = $mainMod, mouse_down, workspace, e+1
      bind = $mainMod, mouse_up, workspace, e-1

      # Move active window to a relative workspace with mainMod + CTRL + ALT + [←→]
      bind = $mainMod CTRL ALT, right, movetoworkspace, r+1
      bind = $mainMod CTRL ALT, left, movetoworkspace, r-1

      # Move active window around current workspace with mainMod + SHIFT + CTRL [←→↑↓]
      bind = $mainMod SHIFT $CONTROL, left, movewindow, l
      bind = $mainMod SHIFT $CONTROL, right, movewindow, r
      bind = $mainMod SHIFT $CONTROL, up, movewindow, u
      bind = $mainMod SHIFT $CONTROL, down, movewindow, d

      # Scroll through existing workspaces with mainMod + scroll
      bind = $mainMod, mouse_down, workspace, e+1
      bind = $mainMod, mouse_up, workspace, e-1

      # Move/Resize windows with mainMod + LMB/RMB and dragging
      bindm = $mainMod, mouse:272, movewindow
      bindm = SHIFT, mouse:272, resizewindow

      bind = $mainMod CTRL, left, resizeactive, -80 0
      bind = $mainMod CTRL, right, resizeactive, 80 0
      bind = $mainMod CTRL, up, resizeactive, 0 -80
      bind = $mainMod CTRL, down, resizeactive, 0 80

       # media and volume controls
      bind = ,XF86AudioRaiseVolume,exec, pamixer -i 2
      bind = ,XF86AudioLowerVolume,exec, pamixer -d 2
      bind = ,XF86AudioMute,exec, pamixer -t
      bind = ,XF86AudioPlay,exec, playerctl play-pause
      bind = ,XF86AudioNext,exec, playerctl next
      bind = ,XF86AudioPrev,exec, playerctl previous
      bind = , XF86AudioStop, exec, playerctl stop

      # Special workspaces (scratchpad)
      bind = CTRL SHIFT, G, movetoworkspacesilent, special
      bind = $mainMod, G, togglespecialworkspace,

      # Toggle Layout
      bind = $mainMod, J, togglesplit, # dwindle

      # Move window silently to workspace Super + Alt + [0-9]
      bind = $mainMod ALT, 1, movetoworkspacesilent, 1
      bind = $mainMod ALT, 2, movetoworkspacesilent, 2
      bind = $mainMod ALT, 3, movetoworkspacesilent, 3
      bind = $mainMod ALT, 4, movetoworkspacesilent, 4
      bind = $mainMod ALT, 5, movetoworkspacesilent, 5
      bind = $mainMod ALT, 6, movetoworkspacesilent, 6
      bind = $mainMod ALT, 7, movetoworkspacesilent, 7
      bind = $mainMod ALT, 8, movetoworkspacesilent, 8
      bind = $mainMod ALT, 9, movetoworkspacesilent, 9
      bind = $mainMod ALT, 0, movetoworkspacesilent, 10

    '';
  };
}
