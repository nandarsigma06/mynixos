{ config, pkgs, lib, ... }:

{
	gtk = {
		enable = true;
		theme = {
			package = pkgs.adw-gtk3;
	    name = "adw-gtk3";
		};
		iconTheme = {
			package = pkgs.papirus-icon-theme;
	    name = "Papirus-Dark";
		};
		cursorTheme = {
			package = pkgs.bibata-cursors;
			name = "Bibata-Modern-Ice";
			size = 24;
		};
	};
}
