
{ config, pkgs, lib, profile, ... }:
{

  programs.waybar = {
    enable = true;
    settings = {

    home.file.".config/waybar/config.ctl" ={
        source = ./waybar/config.ctl;
        execute = true;
        };

    home.file.".config/waybar/wbarconfgen.sh" ={
        source = ./waybar/wbarconfgen.sh;
        execute = true;
        };
    home.file.".config/waybar/wbarstylegen.sh" ={
        source = ./waybar/wbarstylegen.sh;
        execute = true;
        };

    home.file.".config/waybar/config.jsonc".source = ./waybar/config.jsonc;
    home.file.".config/waybar/style.css".source = ./waybar/style.css;

    home.file.".config/waybar/modules/gpuinfo.jsonc".source=./waybar/modules/gpuinfo.jsonc;
    home.file.".config/waybar/modules/window.jsonc".source=./waybar/modules/window.jsonc;
    home.file.".config/waybar/modules/taskbar.jsonc".source=./waybar/modules/taskbar.jsonc;
    home.file.".config/waybar/modules/wallchange.jsonc".source=./waybar/modules/wallchange.jsonc;
    home.file.".config/waybar/modules/mode.jsonc".source=./waybar/modules/mode.jsonc;
    home.file.".config/waybar/modules/footer.jsonc".source=./waybar/modules/footer.jsonc;
    home.file.".config/waybar/modules/cpu.jsonc".source=./waybar/modules/cpu.jsonc;
    home.file.".config/waybar/modules/wbar.jsonc".source=./waybar/modules/wbar.jsonc;
    home.file.".config/waybar/modules/memory.jsonc".source=./waybar/modules/memory.jsonc;
    home.file.".config/waybar/modules/bluetooth.jsonc".source=./waybar/modules/bluetooth.jsonc;
    home.file.".config/waybar/modules/language.jsonc".source=./waybar/modules/language.jsonc;
    home.file.".config/waybar/modules/mpris.jsonc".source=./waybar/modules/mpris.jsonc;
    home.file.".config/waybar/modules/pulseaudio.jsonc".source=./waybar/modules/pulseaudio.jsonc;
    home.file.".config/waybar/modules/network.jsonc".source=./waybar/modules/network.jsonc;
    home.file.".config/waybar/modules/clock.jsonc".source=./waybar/modules/clock.jsonc;
    home.file.".config/waybar/modules/tray.jsonc".source=./waybar/modules/tray.jsonc;
    home.file.".config/waybar/modules/updates.jsonc".source=./waybar/modules/updates.jsonc;
    home.file.".config/waybar/modules/idle_inhibitor.jsonc".source=./waybar/modules/idle_inhibitor.jsonc;
    home.file.".config/waybar/modules/workspaces.jsonc".source=./waybar/modules/workspaces.jsonc;
    home.file.".config/waybar/modules/power.jsonc".source=./waybar/modules/power.jsonc;
    home.file.".config/waybar/modules/header.jsonc".source=./waybar/modules/header.jsonc;
    home.file.".config/waybar/modules/cliphist.jsonc".source=./waybar/modules/cliphist.jsonc;
    home.file.".config/waybar/modules/battery.jsonc".source=./waybar/modules/battery.jsonc;
    home.file.".config/waybar/modules/spotify.jsonc".source=./waybar/modules/spotify.jsonc;
    home.file.".config/waybar/modules/backlight.jsonc".source=./waybar/modules/backlight.jsonc;

    home.file.".config/waybar/themes/Wall-Dcol.css".source=./waybar/themes/Wall-Dcol.css;
    home.file.".config/waybar/themes/Catppuccin-Latte.css".source=./waybar/themes/Catppuccin-Latte.css;
    home.file.".config/waybar/themes/Catppuccin-Mocha.css".source=./waybar/themes/Catppuccin-Mocha.css;
    home.file.".config/waybar/themes/Cyberpunk-Edge.css".source=./waybar/themes/Cyberpunk-Edge.css;
    home.file.".config/waybar/themes/Gruvbox-Retro.css".source=./waybar/themes/Gruvbox-Retro.css;
    home.file.".config/waybar/themes/Material-Sakura.css".source=./waybar/themes/Material-Sakura.css;
    home.file.".config/waybar/themes/Decay-Green.css".source=./waybar/themes/Decay-Green.css;
    home.file.".config/waybar/themes/Frosted-Glass.css".source=./waybar/themes/Frosted-Glass.css;
    home.file.".config/waybar/themes/Tokyo-Night.css".source=./waybar/themes/Tokyo-Night.css;
    home.file.".config/waybar/themes/Graphite-Mono.css".source=./waybar/themes/Graphite-Mono.css;
    home.file.".config/waybar/themes/theme.css".source=./waybar/themes/theme.css;
    home.file.".config/waybar/themes/Rose-Pine.css".source=./waybar/themes/Rose-Pine.css;


    };
}

