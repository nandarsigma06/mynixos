{ config, pkgs, ... }:

{
  services.moodle = {
    package = pkgs.moodle;
    enable = true;
    virtualhost = {
      hostname = "lms.local";
      forceSSL = true;
      documentRoot = "/home/nandar/dcuments/moodle";
    };
  };
  services.moodle.initialPassword = "nandar88";
  services.moodle.database.user = "moodleuser";
  services.moodle.database.type = "pgsql";
  services.moodle.database.name = "moodle";


}
