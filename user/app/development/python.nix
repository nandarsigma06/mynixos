{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
      #python3Full
      (python3.withPackages ( # tambahkan di awal baris ini.
        ps:
          with ps; [
            jupyter
            jupyterlab
            matplotlib
            numpy
            pandas
            plotly
            seaborn
            scikit-learn
            scipy
            sympy
            spyder
          ]
        )
      ) # sampai baris ini.
  ];
}