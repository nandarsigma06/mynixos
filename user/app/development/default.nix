{ ... }: {
  imports = [

    ./python.nix
    #./neovim.nix
    ./fish.nix
    ./git.nix
    ./cc.nix
    ./nix.nix
    #./rstudio.nix
    ./tex.nix
    #./lazygit.nix


  ];
}